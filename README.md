# analysis-base-container-python-venv

Example Dockerfiles and workflows (for both Release 24 and Release 21) for how to create Python virtual environments that **extend** the environments provided in the AnalysisBase Docker images.

## Use

To get these example images either build them yourself locally or

### Release 24

```
docker pull gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:24.2.23
```

### Release 21

```
docker pull gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:21.2.263
```

## Release 24 Example:

The Python 3 virtual environment created extends the environment provided by AnalysisBase and provides the libraries specified in the Release 24 `requirements.lock` lock file.

```console
$ docker run --rm -ti gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:24.2.23
             _ _____ _      _   ___
            /_\_   _| |    /_\ / __|
           / _ \| | | |__ / _ \\__ \
          /_/ \_\_| |____/_/ \_\___/

This is a self-contained ATLAS AnalysisBase image.
To set up the analysis release of the image, please
execute:

          source /release_setup.sh

[bash][atlas]:analysis > . /release_setup.sh
Configured GCC from: /opt/lcg/gcc/11.2.0-8a51a/x86_64-centos7/bin/gcc
Configured AnalysisBase from: /usr/AnalysisBase/24.2.23/InstallArea/x86_64-centos7-gcc11-opt
(venv) [bash][atlas AnalysisBase-24.2.23]:analysis > python -m pip list
Package           Version
----------------- -------
awkward           2.1.1
awkward-cpp       12
Cython            0.29.28
LHAPDF            6.5.1
numpy             1.24.2
packaging         23.0
pip               23.0.1
scipy             1.10.1
setuptools        67.6.0
typing_extensions 4.5.0
uproot            5.0.5
wheel             0.40.0
xgboost           1.7.4
xrootd            5.4.3
(venv) [bash][atlas AnalysisBase-24.2.23]:analysis >
```

## AnalysisBase images

Lists of all AnalysisBase releases are provided on the ATLAS Twikis:

* [Release 24.2.X series](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisBaseReleaseNotes24pt2)
* [Release 21.2.X series](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisBaseReleaseNotes21_2)

More easily though, you can just use [`crane`](https://github.com/google/go-containerregistry/blob/v0.14.0/cmd/crane/) to get a listing of all images from the command line

```
crane ls gitlab-registry.cern.ch/atlas/athena/analysisbase
```
