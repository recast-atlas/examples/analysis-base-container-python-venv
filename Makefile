lock_release_24:
	docker pull gitlab-registry.cern.ch/atlas/athena/analysisbase:24.2.23
	docker build \
		--file release_24/Dockerfile.lockfile-builder \
		--build-arg BASE_IMAGE=gitlab-registry.cern.ch/atlas/athena/analysisbase:24.2.23 \
		--tag gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:rel24-lockfile-builder \
		release_24
	docker run \
		--rm \
		-ti \
		--user 1000:1000 \
		--volume $(shell pwd)/release_24:/workdir \
		gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:rel24-lockfile-builder \
		bash -c 'bash <(curl -sL https://raw.githubusercontent.com/matthewfeickert/cvmfs-venv/v0.0.4/cvmfs-venv.sh) && \
			. venv/bin/activate && \
			python -m pip --no-cache-dir install --upgrade pip-tools && \
			pip-compile --generate-hashes --output-file=requirements.lock requirements.txt && \
			deactivate && \
			rm -r venv'

release_24_image:
	docker pull gitlab-registry.cern.ch/atlas/athena/analysisbase:24.2.23
	docker build \
		--file release_24/Dockerfile \
		--build-arg BASE_IMAGE=gitlab-registry.cern.ch/atlas/athena/analysisbase:24.2.23 \
		--tag gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:24.2.23 \
		release_24

lock_release_21:
	docker pull gitlab-registry.cern.ch/atlas/athena/analysisbase:21.2.263
	docker build \
		--file release_21/Dockerfile.lockfile-builder \
		--build-arg BASE_IMAGE=gitlab-registry.cern.ch/atlas/athena/analysisbase:21.2.263 \
		--tag gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:rel21-lockfile-builder \
		release_21
	docker run \
		--rm \
		-ti \
		--user 1000:1000 \
		--volume $(shell pwd)/release_21:/workdir \
		analysis-base-container-python-venv:rel21-lockfile-builder \
		bash -c '. /release_setup.sh && \
			python -m pip --no-cache-dir install --upgrade pip-tools && \
			pip-compile --generate-hashes --output-file=requirements.lock requirements.txt'

release_21_image:
	docker pull gitlab-registry.cern.ch/atlas/athena/analysisbase:21.2.263
	docker build \
		--file release_21/Dockerfile \
		--build-arg BASE_IMAGE=gitlab-registry.cern.ch/atlas/athena/analysisbase:21.2.263 \
		--tag gitlab-registry.cern.ch/recast-atlas/examples/analysis-base-container-python-venv:21.2.263 \
		release_21
